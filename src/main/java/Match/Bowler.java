package Match;

public class Bowler {
    int runsGiven;

    public void setRunsGiven(int runsGiven) {
        this.runsGiven = runsGiven;
    }

    public boolean hasTakenWicket(BatsmanType batsmanType, int run) {
        if (batsmanType == BatsmanType.TAIL_ENDER_BATSMAN) {
            return (runsGiven % 2 == run % 2);
        }
        return runsGiven == run;
    }
}
