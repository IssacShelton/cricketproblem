package Match;

public class Match {

    private final int overs;

    public static final String BATSMAN_WON = "Batsman has Won";

    public static final String BATSMAN_LOST = "Batsman has Lost";

    static final int BALLS_PER_OVER = 6;

    static final int MAXIMUM_RUNS_IN_A_BALL = 6;
    private final Player playerOne;
    private final Player playerTwo;
    private final RandomRun randomRun;
    public String result;
    private int totalToBeChased;


    public Match(int overs, Player playerOne, Player playerTwo, RandomRun randomRun) {
        this.overs = overs;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.randomRun = randomRun;
    }

    public String getResult() {
        if (playerTwo.batsman.hasChasedTarget(totalToBeChased)) {
            return BATSMAN_WON;
        }
        return BATSMAN_LOST;

    }

    public void play() {
        for(int innings =1;innings<=2;innings++){
            if(innings==1) {
                for (int ball = 0; ball < overs * BALLS_PER_OVER; ball++) {
                    int batsmanRun = randomRun.getRun(BatsmanType.values()[playerOne.getBatsmanType()]);
                    int bowlerRun = randomRun.getRun();
                    playerTwo.bowler.setRunsGiven(bowlerRun);
                    displayScore(innings, batsmanRun, bowlerRun);
                    boolean wicketCheck = playerTwo.bowler.hasTakenWicket(BatsmanType.values()[playerOne.getBatsmanType()], batsmanRun);
                    if (wicketCheck && playerTwo.getBowlerType() == 1) {
                        continue;
                    }
                    if (!wicketCheck) {
                        playerOne.batsman.addScore(batsmanRun);
                    }
                    if(wicketCheck){
                        break;
                    }
                }
                totalToBeChased = (playerOne.batsman.totalRuns) +1;
                System.out.println("Target :"+ totalToBeChased);
            }
            else{
                for (int ball = 0; ball < overs * BALLS_PER_OVER; ball++) {
                    int batsmanRun = randomRun.getRun(BatsmanType.values()[playerTwo.getBatsmanType()]);
                    int bowlerRun = randomRun.getRun();
                    playerOne.bowler.setRunsGiven(bowlerRun);
                    displayScore(innings,batsmanRun,bowlerRun);
                    boolean wicketCheck = playerOne.bowler.hasTakenWicket(BatsmanType.values()[playerTwo.getBatsmanType()], batsmanRun);
                    if (wicketCheck && playerOne.getBowlerType() == 1) {
                        continue;
                    }
                    if (!wicketCheck) {
                        playerTwo.batsman.addScore(batsmanRun);
                    }
                    result = getResult();
                    if (wicketCheck || (result.equals(BATSMAN_WON))) {
                        break;
                    }
                }
            }

        }
        System.out.println(result);
        }


    private void displayScore(int innings,int batsmanRun,int bowlerRun){
        if(innings == 1){
            System.out.println("Batsman1 : " + batsmanRun);
            System.out.println("Bowler2 : " + bowlerRun);
        }
        else{
            System.out.println("Batsman2 : " + batsmanRun);
            System.out.println("Bowler1 : " + bowlerRun);
        }

    }
}


