package Match;

import java.util.Scanner;

public class Begin {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Number of overs: ");
        int overs = scanner.nextInt();
        System.out.println("Player 1");
        System.out.println("Batsman Type: ");
        System.out.println("0.Normal Batsman");
        System.out.println("1.Hitter");
        System.out.println("2.Defensive");
        System.out.println("3.Tail Ender");
        int playerOneBatsmanType = scanner.nextInt();
        System.out.println("Bowler Type: ");
        System.out.println("0.Normal Bowler");
        System.out.println("1.Part time Bowler");
        int playerOneBowlerType = scanner.nextInt();
        System.out.println("Player 2");
        System.out.println("Batsman Type: ");
        System.out.println("0.Normal Batsman");
        System.out.println("1.Hitter");
        System.out.println("2.Defensive");
        System.out.println("3.Tail Ender");
        int playerTwoBatsmanType = scanner.nextInt();
        System.out.println("Bowler Type: ");
        System.out.println("0.Normal Bowler");
        System.out.println("1.Part time Bowler");
        int playerTwoBowlerType = scanner.nextInt();
//        Batsman batsmanOne = new Batsman();
//        Bowler bowlerOne = new Bowler();
//        Batsman batsmanTwo = new Batsman();
//        Bowler bowlerTwo = new Bowler();
        Player playerOne = new Player(playerOneBatsmanType,playerOneBowlerType);
        Player playerTwo = new Player(playerTwoBatsmanType,playerTwoBowlerType);
        RandomRun randomRun = new RandomRun();
        Match match = new Match(overs,playerOne,playerTwo, randomRun);
        match.play();
    }
}
