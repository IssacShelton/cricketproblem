package Match;

public enum BatsmanType {
    NORMAL_BATSMAN(new int[]{0, 1, 2, 3, 4, 5, 6}),
    HITTER_BATSMAN(new int[]{0, 4, 6}),
    DEFENSIVE_BATSMAN(new int[]{0, 1, 2, 3}),
    TAIL_ENDER_BATSMAN(new int[]{0, 1, 2, 3, 4, 5, 6});

    final int[] score;

    BatsmanType(int[] score) {
        this.score = score;
    }
}
