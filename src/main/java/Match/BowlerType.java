package Match;

public enum BowlerType {
    ORDINARY_BOWLER(),
    PART_TIME_BOWLER()
}
