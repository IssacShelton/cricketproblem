package Match;

import java.util.Random;

public class RandomRun {
    public int getRun(BatsmanType type){
        Random random = new Random();
        return  type.score[random.nextInt(type.score.length)];
    }

    public int getRun(){
        Random random = new Random();
        return random.nextInt(Match.MAXIMUM_RUNS_IN_A_BALL +1);
    }
}
