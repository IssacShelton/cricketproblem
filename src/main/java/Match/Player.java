package Match;

public class Player {


    public Batsman batsman;
    public Bowler bowler;
    private final int batsmanType;
    private final int bowlerType;

    public Player(int batsmanType, int bowlerType){
        this.batsmanType = batsmanType;
        this.bowlerType = bowlerType;
    }

    public int getBatsmanType() {
        return batsmanType;
    }

    public int getBowlerType() {
        return bowlerType;
    }
}
