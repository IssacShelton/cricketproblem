import Match.Match;
import Match.fakes.MockBatsman;
import Match.fakes.MockBowler;
import Match.fakes.MockRandomRun;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MatchTest {

//    @Test
//    void shouldReturnBatsmanHasWonWhenNormalBatsmanHasChasedTwoRuns() {
//        int target = 2;
//        int overs = 1;
//        int batsmanType = 0;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(true);
//        MockBowler mockBowler = new MockBowler(false);
//        MockRandomRun mockRandomRun = new MockRandomRun(3, 0);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_WON, result);
//        assertEquals(2,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(1,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(1,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }
//
//    @Test
//    void shouldReturnBatsmanHasLostWhenNormalBatsmanLostHisWicketBeforeChasingFiveRuns() {
//        int target = 5;
//        int overs = 1;
//        int batsmanType = 0;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(false);
//        MockBowler mockBowler = new MockBowler(true);
//        MockRandomRun mockRandomRun = new MockRandomRun(1, 1);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_LOST, result);
//        assertEquals(2,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(0,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(1,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }
//
//    @Test
//    void shouldReturnBatsmanHasWonWhenHitterBatsmanHasChasedThreeRunsInOneOver() {
//        int target = 3;
//        int overs = 1;
//        int batsmanType = 1;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(true);
//        MockBowler mockBowler = new MockBowler(false);
//        MockRandomRun mockRandomRun = new MockRandomRun(4, 0);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_WON, result);
//        assertEquals(2,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(1,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(1,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }
//
//    @Test
//    void shouldReturnBatsmanHasLostWhenHitterBatsmanHasNotChasedTheTarget() {
//        int target = 12;
//        int overs = 1;
//        int batsmanType = 1;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(false);
//        MockBowler mockBowler = new MockBowler(true);
//        MockRandomRun mockRandomRun = new MockRandomRun(6, 6);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_LOST, result);
//        assertEquals(2,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(0,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(1,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }
//
//    @Test
//    void shouldReturnBatsmanHasWonWhenDefensiveBatsmanHasChasedOneRunInOneOver() {
//        int target = 1;
//        int overs = 1;
//        int batsmanType = 2;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(true);
//        MockBowler mockBowler = new MockBowler(false);
//        MockRandomRun mockRandomRun = new MockRandomRun(2, 0);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_WON, result);
//        assertEquals(2,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(1,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(1,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }
//
//
//    @Test
//    void shouldReturnBatsmanHasLostWhenDefensiveBatsmanHasNotChasedTheTarget() {
//        int target = 13;
//        int overs = 1;
//        int batsmanType = 2;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(false);
//        MockBowler mockBowler = new MockBowler(false);
//        MockRandomRun mockRandomRun = new MockRandomRun(2, 3);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_LOST, result);
//        assertEquals(7,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(6,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(6,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(6,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(6,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }
//
//    @Test
//    void shouldReturnBatsmanHasWonWhenTailEnderBatsmanHasChasedTheTarget() {
//        int target = 6;
//        int overs = 1;
//        int batsmanType = 3;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(true);
//        MockBowler mockBowler = new MockBowler(false);
//        MockRandomRun mockRandomRun = new MockRandomRun(6, 5);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_WON, result);
//        assertEquals(2,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(1,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(1,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }
//
//    @Test
//    void shouldReturnBatsmanHasLostWhenTailEnderBatsmanHasNotChasedTheTarget() {
//        int target = 12;
//        int overs = 1;
//        int batsmanType = 3;
//        int bowlerType = 0;
//        MockBatsman mockBatsman = new MockBatsman(false);
//        MockBowler mockBowler = new MockBowler(false);
//        MockRandomRun mockRandomRun = new MockRandomRun(1, 2);
//        Match match = new Match(overs, mockBowler, mockBatsman, target, batsmanType, bowlerType, mockRandomRun);
//
//        String result = match.getResult();
//        match.play();
//
//        assertEquals(Match.BATSMAN_LOST, result);
//        assertEquals(7,mockBatsman.noOfInvocationsOfHasChasedTarget);
//        assertEquals(6,mockBatsman.noOfInvocationsOfAddScore);
//        assertEquals(6,mockBowler.noOfInvocationsOfHasTakenWicket);
//        assertEquals(6,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
//        assertEquals(6,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
//    }

    @Test
    void shouldReturnPlayerTwoHasLostWhenPlayerTwoHasNotChasedTheTarget() {
        int overs = 1;
        int playerOneBatsmanType = 2;
        int playerOneBowlerType = 0;
        int playerTwoBatsmanType = 3;
        int playerTwoBowlerType = 1;
        MockBatsman mockBatsman = new MockBatsman(false);
        MockBowler mockBowler = new MockBowler(false);
        MockRandomRun mockRandomRun = new MockRandomRun(1, 2);
        Match match = new Match(overs, mockBowler, mockBatsman,playerOneBatsmanType,playerOneBowlerType,playerTwoBatsmanType,playerTwoBowlerType, mockRandomRun);

        String result = match.getResult();
        match.play();

        assertEquals(Match.BATSMAN_LOST, result);
        assertEquals(7,mockBatsman.noOfInvocationsOfHasChasedTarget);
        assertEquals(6,mockBatsman.noOfInvocationsOfAddScore);
        assertEquals(6,mockBowler.noOfInvocationsOfHasTakenWicket);
        assertEquals(6,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
        assertEquals(6,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
    }

    @Test
    void shouldReturnPlayerTwoHasWonWhenPlayerTwoHasChasedTheTarget() {
        int overs = 1;
        int playerOneBatsmanType = 1;
        int playerOneBowlerType = 1;
        int playerTwoBatsmanType = 2;
        int playerTwoBowlerType = 1;
        MockBatsman mockBatsman = new MockBatsman(true);
        MockBowler mockBowler = new MockBowler(false);
        MockRandomRun mockRandomRun = new MockRandomRun(6, 5);
        Match match = new Match(overs, mockBowler, mockBatsman,playerOneBatsmanType,playerOneBowlerType,playerTwoBatsmanType,playerTwoBowlerType, mockRandomRun);

        String result = match.getResult();
        match.play();

        assertEquals(Match.BATSMAN_WON, result);
        assertEquals(2,mockBatsman.noOfInvocationsOfHasChasedTarget);
        assertEquals(1,mockBatsman.noOfInvocationsOfAddScore);
        assertEquals(1,mockBowler.noOfInvocationsOfHasTakenWicket);
        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBatsmanRun);
        assertEquals(1,mockRandomRun.noOfInvocationsOfGenerateBowlerRun);
    }
}
