import Match.BatsmanType;
import Match.Bowler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BowlerTest {

    @Test
    void shouldReturnTrueWhenBowlerHasTakenAWicket() {
        Bowler bowler = new Bowler();
        bowler.setRunsGiven(3);
        int runs = 3;
        int batsmanType = 1;

        boolean actual = bowler.hasTakenWicket(BatsmanType.values()[batsmanType], runs);

        assertTrue(actual);
    }

    @Test
    void shouldReturnFalseWhenBowlerHasNotTakenAWicket() {
        Bowler bowler = new Bowler();
        bowler.setRunsGiven(5);
        int runs = 4;
        int batsmanType = 1;

        boolean actual = bowler.hasTakenWicket(BatsmanType.values()[batsmanType], runs);

        assertFalse(actual);
    }
}