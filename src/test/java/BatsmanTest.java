import Match.Batsman;
import Match.Bowler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BatsmanTest {

    @Test
    void shouldReturnFourWhenBatsmanRunsIsGiven() {
        Batsman batsman = new Batsman();
        int expected = 4;

        int actual = batsman.addScore(4);

        assertEquals(expected, actual);
    }


    @Test
    void shouldReturnTrueWhenBatsmanHasChasedTheTarget() {
        int target = 8;
        Batsman batsman = new Batsman();
        batsman.addScore(4);
        batsman.addScore(5);

        boolean actual = batsman.hasChasedTarget(target);

        assertTrue(actual);
    }

    @Test
    void shouldReturnFalseWhenBatsmanHasNotChasedTheTarget() {
        int target = 12;
        Batsman batsman = new Batsman();
        batsman.addScore(4);
        batsman.addScore(5);

        boolean actual = batsman.hasChasedTarget(target);

        assertFalse(actual);
    }


}