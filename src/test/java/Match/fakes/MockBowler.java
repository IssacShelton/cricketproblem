package Match.fakes;

import Match.BatsmanType;
import Match.Bowler;

public class MockBowler extends Bowler {
    private final boolean wicketStatus;
    public int noOfInvocationsOfHasTakenWicket = 0;

    public MockBowler(boolean wicketStatus) {
        this.wicketStatus = wicketStatus;
    }

    public boolean hasTakenWicket(BatsmanType batsmanType, int run) {
        noOfInvocationsOfHasTakenWicket++;
        return wicketStatus;
    }
}