package Match.fakes;

import Match.BatsmanType;
import Match.RandomRun;

public class MockRandomRun extends RandomRun {

    private final int batsmanRun;
    private final int bowlerRun;
    public int noOfInvocationsOfGenerateBatsmanRun = 0;
    public int noOfInvocationsOfGenerateBowlerRun = 0;

    public MockRandomRun(int batsmanRun, int bowlerRun) {
        this.batsmanRun = batsmanRun;
        this.bowlerRun = bowlerRun;
    }

    public int generateBatsmanRun(BatsmanType batsmanType) {
        noOfInvocationsOfGenerateBatsmanRun++;
        return batsmanRun;
    }

    public int generateBowlerRun() {
        noOfInvocationsOfGenerateBowlerRun++;
        return bowlerRun;
    }
}
