package Match.fakes;

import Match.Batsman;

public class MockBatsman extends Batsman {
    boolean targetChaseStatus;
    public int noOfInvocationsOfAddScore = 0;
    public int noOfInvocationsOfHasChasedTarget = 0;

    public MockBatsman(boolean targetChaseStatus) {
        this.targetChaseStatus = targetChaseStatus;
    }

    public int addScore(int totalRuns) {
        noOfInvocationsOfAddScore++;
        return totalRuns;
    }

    public boolean hasChasedTarget(int target) {
        noOfInvocationsOfHasChasedTarget++;
        return targetChaseStatus;
    }
}
